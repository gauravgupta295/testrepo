// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Suggestion__ChangeEvent {
    global Object ChangeEventHeader;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Id Id;
    global Date Implemented_Date__c;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global Double Number_of_Days_Open__c;
    global SObject Owner;
    global Id OwnerId;
    global String ReplayId;
    global String Status__c;
    global String Suggestion_Category__c;
    global String Suggestion_Description__c;

    global Suggestion__ChangeEvent () 
    {
    }
}