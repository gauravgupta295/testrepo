// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Product2ChangeEvent {
    global Object ChangeEventHeader;
    global Decimal Cost__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Double Current_Inventory__c;
    global String Description;
    global String DisplayUrl;
    global ExternalDataSource ExternalDataSource;
    global Id ExternalDataSourceId;
    global String ExternalId;
    global String Family;
    global Id Id;
    global Boolean IsActive;
    global Boolean IsArchived;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Double Lifespan_Months__c;
    global String Macaron_Flavor__c;
    global Double Maintenance_Cycle__c;
    global String Name;
    global String ProductCode;
    global String QuantityUnitOfMeasure;
    global Boolean Replacement_Part__c;
    global String ReplayId;
    global String Shirt__c;
    global String StockKeepingUnit;
    global String Warehouse_SKU__c;

    global Product2ChangeEvent () 
    {
    }
}