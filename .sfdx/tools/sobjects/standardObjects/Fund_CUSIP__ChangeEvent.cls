// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Fund_CUSIP__ChangeEvent {
    global Object ChangeEventHeader;
    global String Company_Contact_List__c;
    global String Contact_Information__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Fund_CUSIP__c;
    global Id Fund_Company__c;
    global String Fund_Ticker__c;
    global Id Id;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global String ReplayId;
    global String Standard_Buy_Cutoff_Time__c;
    global String Standard_Fund_Buy_Sell_Time_Min__c;
    global String Standard_Fund_Buy_Time_Hr__c;
    global String Standard_Fund_Sell_Time_Hr__c;
    global String Standard_Fund_Sell_Time_Min__c;
    global String Standard_Fund_Wire_Time_Hr__c;
    global String Standard_Fund_Wire_Time_Min__c;
    global String Standard_Sell_Cutoff_Time__c;
    global String Standard_Wire_Cutoff_Time__c;

    global Fund_CUSIP__ChangeEvent () 
    {
    }
}