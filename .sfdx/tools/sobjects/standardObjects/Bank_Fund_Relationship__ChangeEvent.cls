// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Bank_Fund_Relationship__ChangeEvent {
    global Bank_CUSIP__c Bank_CUSIP__c;
    global Id Bank_Code__c;
    global Object ChangeEventHeader;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Fund_CUSIP__c Fund_CUSIP__c;
    global String Group__c;
    global Id Id;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global String Repeat_Code__c;
    global String ReplayId;
    global Double Trade_Authentication_Id__c;

    global Bank_Fund_Relationship__ChangeEvent () 
    {
    }
}