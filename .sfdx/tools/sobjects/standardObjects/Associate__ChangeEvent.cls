// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Associate__ChangeEvent {
    global String Address__c;
    global Object ChangeEventHeader;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Email__c;
    global String Full_Name__c;
    global String Gender__c;
    global Id Id;
    global Date Joining_Date__c;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global SObject Owner;
    global Id OwnerId;
    global String Phone_Number__c;
    global String ReplayId;
    global String Status__c;

    global Associate__ChangeEvent () 
    {
    }
}