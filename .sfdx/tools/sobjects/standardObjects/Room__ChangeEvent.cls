// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Room__ChangeEvent {
    global Boolean Accessbility__c;
    global Object ChangeEventHeader;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Floor__c;
    global Id Id;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Max_Occupancy__c;
    global String Name;
    global SObject Owner;
    global Id OwnerId;
    global Decimal Price__c;
    global String ReplayId;
    global String Status__c;
    global String Type__c;

    global Room__ChangeEvent () 
    {
    }
}