// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Fund_Company__ChangeEvent {
    global Object ChangeEventHeader;
    global String Company_Email__c;
    global String Company_Fax__c;
    global String Company_Name__c;
    global String Company_Website__c;
    global String Contact_No__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Id Id;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global SObject Owner;
    global Id OwnerId;
    global String ReplayId;

    global Fund_Company__ChangeEvent () 
    {
    }
}