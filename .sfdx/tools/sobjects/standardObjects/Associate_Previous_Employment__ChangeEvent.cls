// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Associate_Previous_Employment__ChangeEvent {
    global String Address__c;
    global Id Associate__c;
    global Object ChangeEventHeader;
    global String Contact__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Double Experience_In_Months__c;
    global Id Id;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global String Name__c;
    global String ReplayId;

    global Associate_Previous_Employment__ChangeEvent () 
    {
    }
}