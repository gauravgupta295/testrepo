// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Fund_CUSIP__c {
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<Bank_Fund_Relationship__c> Bank_Fund_Relationships__r;
    global List<CombinedAttachment> CombinedAttachments;
    global String Company_Contact_List__c;
    global List<ContactRequest> ContactRequests;
    global String Contact_Information__c;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global String Fund_CUSIP__c;
    global Id Fund_Company__c;
    global Fund_Company__c Fund_Company__r;
    global String Fund_Ticker__c;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<FeedItem> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global List<EventRelationChangeEvent> Relation;
    global String Standard_Buy_Cutoff_Time__c;
    global String Standard_Fund_Buy_Sell_Time_Min__c;
    global String Standard_Fund_Buy_Time_Hr__c;
    global String Standard_Fund_Sell_Time_Hr__c;
    global String Standard_Fund_Sell_Time_Min__c;
    global String Standard_Fund_Wire_Time_Hr__c;
    global String Standard_Fund_Wire_Time_Min__c;
    global String Standard_Sell_Cutoff_Time__c;
    global String Standard_Wire_Cutoff_Time__c;
    global Datetime SystemModstamp;
    global List<TopicAssignment> TopicAssignments;
    global List<EventChangeEvent> What;

    global Fund_CUSIP__c () 
    {
    }
}