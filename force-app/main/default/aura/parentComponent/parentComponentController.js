({

    doAddition: function(component,event,helper)
    {
       var value1 = parseInt(component.get("v.value1"));
       var value2 = parseInt(component.get("v.value2"));
       component.set("v.sum",value1+value2);

    }
})