trigger AssociateApproval on Associate__c (before update) {

     Map<Id, Associate__C> lstAssociatesIds = new Map<Id, Associate__C>{};
    for(Associate__C a : trigger.new)
    {
        Associate__C oldCur = Trigger.oldMap.get(a.Id);
        if(a.Status__c != oldCur.Status__C)
        {            
           lstAssociatesIds.put(a.Id, a);  
        }
    }
    System.debug(lstAssociatesIds);
   if (!lstAssociatesIds.isEmpty())
   {
       List<id> ApprovalProcessIds;
       for (Associate__c curn : [SELECT 
                                      (SELECT ID FROM ProcessInstances WHERE Status='Approved' ORDER BY CreatedDate DESC LIMIT 1)
                                      FROM Associate__c WHERE ID IN :lstAssociatesIds.keyset()])

    {
        ApprovalProcessIds.add(curn.ProcessInstances[0].Id);
    }      
        
       System.debug(ApprovalProcessIds);
    for (ProcessInstance pi : [SELECT TargetObjectId,
                                   (SELECT Id, StepStatus, Comments FROM Steps ORDER BY CreatedDate DESC LIMIT 1 )
                               FROM ProcessInstance WHERE Id IN :ApprovalProcessIds ORDER BY CreatedDate DESC]) 
    {                     
          System.debug(pi.Steps[0].Comments);
      if ((pi.Steps[0].Comments == null || 
           pi.Steps[0].Comments.trim().length() == 0))
      { System.debug('Please provide a reason!');
          lstAssociatesIds.get(pi.TargetObjectId).addError('Please provide a reason!');
         
      }
    }  
   }
}