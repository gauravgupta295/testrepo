public class UploadFundCUSIP {

    public blob contentFile{get;set;}
    public string FileContent{get;set;}
    String[] filelines = new String[]{};
    List<Bank_CUSIP__c> bankCUSIPList= new List<Bank_CUSIP__c>();    
    public Pagereference  UploadFile()
    {
        PageReference bankCUSIPListPage = null;	
        try
        {
            
             FileContent =blobToString(contentFile,'ISO-8859-1');
             filelines = FileContent.split('\n');
            system.debug(filelines.size());
            for(integer i =1 ; i< filelines.size();i++)
            {
                 
                 String[] inputvalues = new String[]{};
                 inputvalues = filelines[i].split(',');
                 Bank_CUSIP__c cusip= new Bank_CUSIP__c();
                 Bank__c bankobj= [select  id from Bank__c where Bank_Code__c =: integer.valueof(inputvalues[0])];
                 cusip.Bank__c = bankobj.id;
                 cusip.Bank_CUSIP__c = inputvalues[1];
                 cusip.Bank_Ticker__c = inputvalues[2];
                 bankCUSIPList.add(cusip);
            }    
           insert bankCUSIPList;
           Schema.DescribeSObjectResult result = Bank_CUSIP__c.SObjectType.getDescribe();
           bankCUSIPListPage =  new PageReference('/' + result.getKeyPrefix() + '/o');
           bankCUSIPListPage.setRedirect(true);          		  
        }
        catch(Exception e)
        {
              ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
              ApexPages.addMessage(errormsg);            
        }
        
         return  bankCUSIPListPage;
    }
    
     public static String blobToString(Blob input, String inCharset)
     {
        String hex = EncodingUtil.convertToHex(input);        
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    } 
}