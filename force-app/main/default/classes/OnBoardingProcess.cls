Public Class OnBoardingProcess
{
  Public AssociateEntity AssociateWrapper{get;set;}
  private ApexPages.StandardController controller;
  Public Associate_Previous_Employment__c EmployementHistory{get;set;}
  public Attachment objAttachmentDocs{get;set;}
  
  private Id id;
  public OnBoardingProcess(ApexPages.StandardController ctr)
  {
     controller = ctr;     
     id = ApexPages.currentPage().getParameters().get('Id');
     EmployementHistory = new Associate_Previous_Employment__c();
     objAttachmentDocs =  new Attachment();
     AssociateWrapper = new AssociateEntity(ApexPages.currentPage().getParameters().get('Id'));         
  }
    
    public void AddEmploymentHistory()
    {                   
       EmployementHistory.Associate__C = id;       
       AssociateWrapper.AssociatePreviousEmployment.add(EmployementHistory);
       EmployementHistory = new Associate_Previous_Employment__c();       
    }
    
    public PageReference Submit()
    {  
      try
      {
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(AssociateWrapper.AssociatePersonalInfo.Id);
            // submit the approval request for processing
            Approval.ProcessResult result = Approval.process(req); 
      }
      catch(Exception ex)
      {
          System.Debug('Error Found - Transaction rollback'+ex);
           ApexPages.addMessages(ex);
      }
            return null;
    }
    
  public PageReference Save()
  {
       
     Savepoint sp = Database.setSavepoint();
     PageReference returnValue ;
      try
      {          
         upsert AssociateWrapper.AssociatePersonalInfo;
          if(id == null)
          {
            AssociateWrapper.AssociateCompanyInfo.Associate__c = AssociateWrapper.AssociatePersonalInfo.Id;      
            for(Associate_Previous_Employment__c History :   AssociateWrapper.AssociatePreviousEmployment)
            {   
            History.Associate__c= AssociateWrapper.AssociatePersonalInfo.Id;     
            }                                             
          }
     
         upsert AssociateWrapper.AssociateCompanyInfo;
            
         upsert AssociateWrapper.AssociatePreviousEmployment;  
          AssociateWrapper.AssociateDocs.Associate__C = AssociateWrapper.AssociatePersonalInfo.Id;  
         upsert AssociateWrapper.AssociateDocs;
         
         objAttachmentDocs.ParentId = AssociateWrapper.AssociateDocs.Id;
         
         upsert ObjAttachmentDocs;
         
        returnValue   = new PageReference('/' + AssociateWrapper.AssociatePersonalInfo.Id);
     }
     catch(DmlException ex)
     {
       Database.rollback(sp);
       System.Debug('Error Found - Transaction rollback'+ex);
       ApexPages.addMessages(ex);
       returnValue =null;
     }
     return returnValue ;
  }
     
}