Public Class AssociateEntity
{
   Public Associate__c AssociatePersonalInfo {get;set;}
   Public List<Associate_Previous_Employment__c> AssociatePreviousEmployment {get;set;}
   
   Public Associate_Company_Information__c AssociateCompanyInfo{get; set;}
   Public Associate_Docs__c AssociateDocs{get;set;}      
   
   public AssociateEntity(Id id)
   {
    if(id == null)
     {
       this.AssociatePersonalInfo =new Associate__c();
       this.AssociatePreviousEmployment =new List<Associate_Previous_Employment__c>();
      
       this.AssociateCompanyInfo = new Associate_Company_Information__c();
       this.AssociateDocs = new Associate_Docs__c();
     }
     else
     {      
       LoadEditData(id);
     }
   
   }
   
   private void LoadEditData(Id id)
   {
     this.AssociatePersonalInfo =[select Full_Name__c,Address__c,Email__c,Gender__c,Joining_Date__c,Phone_Number__c from Associate__C where Id=: id];
     this.AssociatePreviousEmployment =[select Associate__C , Address__c, Contact__c, Name__c,Experience_In_Months__c from Associate_Previous_Employment__c where Associate__c =: id];
     this.AssociateCompanyInfo = [select Associate__C , Department__c, Email__c, Name__c from Associate_Company_Information__c where Associate__c =: id];    
     integer DocsCount = [select count() from Associate_Docs__c where Associate__C =: id];
     if(DocsCount == 0)
     { 
        this.AssociateDocs = new Associate_Docs__c();
     }
     else
     {
          this.AssociateDocs = [select Associate__c, name__c from Associate_Docs__c where Associate__C =: id];
     }
   }
}